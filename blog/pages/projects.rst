.. title: Projects
.. date: 2022-03-20 14:03:00

This page showcases some of the stuff I work on from time to time. The projects are not in any particular order. You can find work on GitHub_.

.. _GitLab: https://gitlab.com/zhgoh
.. _GitHub: https://github.com/zhgoh
.. contents:: Listings

###
C++
###

One of the first language I learn, back then it was still C++ 11. We built our own games and engines back in Digipen, here are some of the stuff we worked on. If you are interested,
do check out the gallery links to our games.

================
Super Combo Time
================

My first game project, you play as a pirates, ahoy mates. Cooperate with your other fellow mates and bring home the good old treasure, aye aye.

.. image:: https://games.digipen.edu/system/photos/3084/original/Super_Combo_Time_1.png
   :width: 1024
   :height: 768
   :scale: 45%
.. image:: https://games.digipen.edu/system/photos/3088/original/Super_Combo_Time_2.png
   :width: 1024
   :height: 768
   :scale: 45%

Genre: Twinstick shooter, Co-op

DigiPen Gallery: `Super Combo Time`_

Tech: C++, Project Fun(Provided by DigiPen)

.. _Super Combo Time: https://games.digipen.edu/games/super-combo-time

=========
Discharge
=========

This is our second game which we build using DigiPen's Alpha engine (which is a graphics framework). You play as discharge the robot as you figure the way out,
beware of those pesky moleman who might just wants a piece of you. This is a turn based puzzle game where you solve puzzle and make it to the end of the level.

.. image:: https://games.digipen.edu/system/photos/3632/original/Discharge_1.jpg
   :width: 1024
   :height: 768
   :scale: 45%
.. image:: https://games.digipen.edu/system/photos/3635/original/Discharge_2.jpg
   :width: 1024
   :height: 768
   :scale: 45%
.. image:: https://games.digipen.edu/system/photos/3638/original/Discharge_3.jpg
   :width: 1024
   :height: 768
   :scale: 45%

Genre: Turn based, Puzzle

DigiPen Gallery: `Discharge`_

Tech: C++, Alpha Engine (Provided by DigiPen)

.. _Discharge: https://games.digipen.edu/games/discharge

============
Basket Brawl
============

Welcome to the sports of the century!! This is a 2D platformer fast-paced sports game where players fight against one another to score as many points into the hoop as possible.

.. image:: https://games.digipen.edu/system/photos/5568/original/BasketBrawl_2.png
   :width: 1920
   :height: 1200
   :scale: 25%
.. image:: https://games.digipen.edu/system/photos/5571/original/BasketBrawl_3.png
   :width: 1920
   :height: 1200
   :scale: 25%
.. image:: https://games.digipen.edu/system/photos/5574/original/BasketBrawl_4.png
   :width: 1920
   :height: 1200
   :scale: 25%

Genre: Action, Sports

DigiPen Gallery: `Basket Brawl`_

Tech: C++, Imgui, DirectX 11, Custom Engine

.. _Basket Brawl: https://games.digipen.edu/games/basket-brawl

=================
The Last Defender
=================

Our final year project, we built a 3D third-person tower defence game.

.. image:: https://games.digipen.edu/system/photos/7649/original/Last_Defender_1.png
   :width: 1024
   :height: 768
   :scale: 45%
.. image:: https://games.digipen.edu/system/photos/7652/original/Last_Defender_2.png
   :width: 1024
   :height: 768
   :scale: 45%

Genre: Tower defence

DigiPen Gallery: `The Last Defender`_

Tech: C++, Imgui, OpenGL, Custom Engine

.. _The Last Defender: https://games.digipen.edu/games/the-last-defender

======================
Encrypted Mail Fetcher
======================

I wrote a command line interface application to fetch emails from Google mail
using Google's C++ sdk.
After which it uses openssl library to encrypt the messages.

Link: `gitlab/MailFetcher`_

Tech: C++, openssl

.. _gitlab/MailFetcher: https://gitlab.com/zhgoh/MailFetcher

######
Python
######

One of my favourite language to use, I have used it for multiple web scraping projects and also together with Jupyter Notebook to do some data visualization
stuff. Some of my projects are as follows.

=======
My Blog
=======

Howdy, the page you are seeing is a work of mine, albeit with a little help from Nikola for generating the page and gitlab and github for hosting it.

Tech: Nikola, reStructuredText, Python, HTML, CSS

.. _reStructuredText: https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html
.. _Nikola: https://getnikola.com/

================
WhatCard Scraper
================

One afternoon, I decided to scrape all the Merchant Category Code (MCC) data from this page, WhatCard and I decided to write a script to fetch all the data. It was my first time that
I have encountered a website that loads data using websocket. Hence I wrote a simple Python script to simulate the interactions between the web browser and the server to see how the
data was being requested.

Link: `gitlab/whatcard_scrape`_

Tech: Python, Websocket

.. _gitlab/whatcard_scrape: https://gitlab.com/zhgoh/whatcard_scrape

===============
3D Buzz Scraper
===============

I posted about this some time ago, when the popular 3D content website 3D Buzz was going to be shut down. They released most of their contents for free download for a limited period of
time, I took the liberty and wrote a script to download all of that contents.

Link: `gitlab/3dbuzz_content_scraper`_

Tech: Python, BeautifulSoup, requests

.. _gitlab/3dbuzz_content_scraper: https://gitlab.com/zhgoh/3dbuzz_content_scraper

================
Packt Downloader
================

Packtpub has released weekly free ebooks for download, I decided to write a Python script using requests and aiohttp library to download all the titles I have in my library. I no
longer use the script and is currently unmaintained.

Link: `gitlab/packt_downloader`_

Tech: Python, aiohttp, requests

.. _gitlab/packt_downloader: https://gitlab.com/zhgoh/packt_downloader
.. _aiohttp: https://docs.aiohttp.org/en/stable/
.. _requests: https://docs.python-requests.org/en/latest/

===============================
Housing visualization dashboard
===============================

.. image:: https://gitlab.com/zhgoh/hdb_resale_visualization/-/raw/main/Screenshots/Screenshots.png

There was a period of time where I got interested in data visualization and also I wanted to play around with building a dashboard of data visualization. I got to building a dashboard
for fun. The data are provided by `data.gov.sg`_, using the Housing Development Board (HDB) resale price. Using the Dash library to build the dashboard and the visualization, currently
the project is hosted on my `Heroku`_.

Link: `gitlab/hdb_resale_visualization`_

Tech: Python, Dash

.. _gitlab/hdb_resale_visualization: https://gitlab.com/zhgoh/hdb_resale_visualization
.. _data.gov.sg: https://data.gov.sg/
.. _Heroku: https://damp-stream-44319.herokuapp.com/

######
Golang
######

============================
Uniswap transactions backend
============================

Given as a task for one of my interviews to build a web backend for parsing crypto pair prices on Uniswap. I used Go to build the web backend and uses etherscan's
and Binance's api to get the price data for the blockchain data.

Link: `gitlab/uniswap_transaction_service`_

Tech: Go, Sqlite

.. _gitlab/uniswap_transaction_service: https://gitlab.com/zhgoh/uniswap_transaction_service

####
Java
####

================
Teacher Database
================

.. image:: https://gitlab.com/zhgoh/teacher-database/-/raw/main/Screenshots/Screenshots.PNG
   :width: 970
   :height: 708
   :scale: 80%

This project was built in a couple of days as a project to teach one of my student Java. It teaches student a few things, from displaying a text menu to state management as
well as saving/loading files from disk to displaying those data on the screen.

Link: `gitlab/teacher-database`_

Tech: Java

.. _gitlab/teacher-database: https://gitlab.com/zhgoh/teacher-database

=====
Chess
=====

.. image:: https://gitlab.com/zhgoh/jchess/-/raw/main/Screenshots/Screenshots.PNG
   :width: 1018
   :height: 658
   :scale: 80%

Building a (simple) Java chess game in Java to teach myself some basics of Java as well as to use it as a teaching material to teach my student about Java. The game is very simple
with no AI and is meant to be played with 2 players. The chess program accepts inputs to move the pieces, it supports some basic chess moves like castling and pawns moving 1/2
spaces at the beginning.

Link: `gitlab/jchess`_

Tech: Java

.. _gitlab/jchess: https://gitlab.com/zhgoh/jchess

===========
Tic Tac Toe
===========

.. image:: https://gitlab.com/zhgoh/jtictactoe/-/raw/main/Screenshots/Screenshots.PNG

Building a (simple) chess game in Java to teach myself some basics of Java as well as to use it as a teaching material to teach my student about Java. The game is very simple
with no AI and is meant to be played with 2 players. The chess program accepts inputs to move the pieces, it supports some basic chess moves like castling and pawns moving 1/2
spaces at the beginning.

Link: `gitlab/jtictactoe`_

Tech: Java

.. _gitlab/jtictactoe: https://gitlab.com/zhgoh/jtictactoe


######
Dlang
######

=============
Space Invader
=============

.. image:: https://gitlab.com/zhgoh/d_space_invaders/-/raw/master/screenshots/Space_Invaders.png

Saw an article about making a space invaders game from scratch, at that time, I was into the programming language D, and I decided to write a simple game with it. it makes uses of the
OpenGL binding library, derelict.


Link: `gitlab/d_space_invaders`_

Tech: D

.. _gitlab/d_space_invaders: https://gitlab.com/zhgoh/d_space_invaders

===============
Chip-8 Emulator
===============

.. image:: https://gitlab.com/zhgoh/d_chip8/-/raw/master/screenshots/Pong.png

I got interested in this programming language D, and decided to write a fairly simple application to learn more about D and emulations in general. I strongly recommend for anybody
who's interested in emulation to write their own Chip-8 implementations.

Link: `gitlab/d_chip8`_

Tech: D

.. _gitlab/d_chip8: https://gitlab.com/zhgoh/d_chip8

==========
FTP client
==========

A simple FTP client written in D to recap and learn more about sockets programming. This is also an exercise to understand more about the capability of D. Allows connecting to
FTP server and allow of downloading of files.

Link: `gitlab/d_ftp_client`_

Tech: D

.. _gitlab/d_ftp_client: https://gitlab.com/zhgoh/d_ftp_client

###
Nim
###

====================
Devour (Port to nim)
====================

Useful scripts for hiding all x windows when opening certain applications, check the original devour_ repo for more info. This is just a port of devour to nim,
because why not?

Link: `gitlab/devour_nim`_

Tech: Nim

.. _devour: https://github.com/salman-abedin/devour
.. _gitlab/devour_nim: https://gitlab.com/zhgoh/devour_nim

####
Haxe
####

===================
ZE2D Game Framework
===================

.. image:: https://gitlab.com/zhgoh/ze2d/-/raw/master/ZE2D.png

This project is my attempt at learning to build my own game framework using `Haxe`_ programming language and the `OpenFL`_ framework. I think it consists of simple state management and
entity management in the framework. Currently the project is being archived, maybe one day I will go back and explore `Haxe`_.

Link: `gitlab/zE2D`_

Tech: Haxe, OpenFL

.. _OpenFL: https://www.openfl.org/_
.. _Haxe: https://haxe.org/
.. _gitlab/ZE2D: https://gitlab.com/zhgoh/ze2d

====================
The Little Astronaut
====================

.. image:: https://www.dropbox.com/s/074qw3liaq0v4cg/2014-06-07%2015_24_09-Adobe%20Flash%20Player%2013.png?raw=1

My friend Kelvin did the awesome art & design while I do the coding for the game in this game jam. I believe this is our first game jam together.

Tech: Haxe, OpenFL

======
Sombre
======

.. image:: https://www.dropbox.com/s/zc5y2o37gzilqfd/Sombre_0.png?raw=1
   :width: 1281
   :height: 821
   :scale: 50%

Another DBS game jam where my friend Kelvin did the art & design for me again while I did the coding for the game.

Tech: Haxe, OpenFL


####################
Flash ActionScript 3
####################

Flash is a thing of the past, it used to be one of the more popular way to make games and interactive media. Nowadays it belongs to the museum. I started making
games for Flash.

===========
Math Digger
===========

.. image :: https://www.dropbox.com/s/p2d3p2i7rf0t8ms/2014-01-04%2013_42_13-Adobe%20Flash%20Player%2011.png?raw=1
   :width: 658
   :height: 556
   :scale: 50%

One of my many school projects, this was built with Flashpunk library. It was great fun, the art was drawn by me and music was generated with `sfxr`_ or `bfxr`_, I
can't remember which. The game was largely inspired by motherload (another flash game) about digging for treasures.

.. image :: https://www.dropbox.com/s/8bjgfani7lc4toq/2014-01-04%2013_45_04-Adobe%20Flash%20Player%2011.png?raw=1
   :width: 658
   :height: 556
   :scale: 50%

.. image :: https://www.dropbox.com/s/muy9uo34rax7a4g/2014-01-04%2013_45_10-Adobe%20Flash%20Player%2011.png?raw=1
   :width: 658
   :height: 556
   :scale: 50%

.. _sfxr: https://www.drpetter.se/project_sfxr.html
.. _bfxr: https://www.bfxr.net/
