.. title: How to ssh into VMware Player/VirtualBox (*BSD/Linux guest from Windows host)
.. slug: how-to-ssh-into-vmware-player-virtualbox-bsd-linux-guest-from-windows-host
.. date: 2022-04-15 15:40:47 UTC+08:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text

This is a guide for setting up ssh into a virtual machine with \*BSD/Linux host on a Windows host. Depending on the VM you
are using, refer to the instructions below.

Installation
============

Firstly, install the vm of choice, then proceed to create the vm and install the guest os. In this guide, I will not go
into details of the installation process. However the important thing is make sure **openssh server is installed
and enabled on the guest os**.

Prerequisites
-------------
1. Install VM of choice.
2. Install desired OS in VM.
3. Install and make sure openssh is running.
4. Make sure there is an account to ssh into, (not including root account, root account requires extra config, default ssh do not allow root logins).

VirtualBox
==========

We will start off with VirtualBox since this is slightly easier to configure. Go below for instructions for VMware Player.

Setting up SSH access to a VirtualBox host is relatively easy. Just select the vm, go into Settings > Network > Adapter 1 > Port Forwarding. To use port forwarding, ensure
the network is attached to NAT.

.. image:: /images/how-to-ssh-into-vmware-player-virtualbox-bsd-linux-guest-from-windows-host_0.png

Clicking on portforwarding will open this window.

.. image:: /images/how-to-ssh-into-vmware-player-virtualbox-bsd-linux-guest-from-windows-host_1.png

Click on add new rule with the following settings

.. code-block:: text

                name: Allow SSH
                protocol: TCP
                Host IP: <Your Windows public IP>
                Host port: The port the remote SSH connects to, for instance, 2244
                Guest IP: The IP of your guest OS
                Guest Port: it’s TCP 22 for SSH

VMware Player
=============

Setting up portforwarding on VMware player requires a little more work as VMware does not have an UI tool to do this.

To start, we have to edit the following file `C:\\ProgramData\\VMware\\vmnetnat.conf` with admin rights, interestingly in the file, there is a sample line on how to configure for
several web services including SSH. Find for the line SSH and edit it to look something like this.

.. code-block:: text

                <port> = <guest ip>:<ssh port>
                4422   = 192.168.122.128:22

                where port is the port to be exposed on the host machine, and guest ip is the ip address of the guest. ssh port should be default at 22.

Once the file is saved, make sure to restart the following service, either using the command line below

.. code-block:: powershell

                net stop "VMWare NAT Service"
                net start "VMWare NAT Service"

or using services.msc with the service name, VMware NAT Service.

.. image:: /images/how-to-ssh-into-vmware-player-virtualbox-bsd-linux-guest-from-windows-host_2.png


SSH into VM
===========

Once the above is done done, start the VM and connect to it via the following command

.. code-block:: bash

                ssh -p <port set earlier, e.g. 2244> <myusername>@localhost

                # An example of connecting to my created username david at port 2244
                ssh -p 2244 david@localhost


Credits to Stanley for the medium_ post.

.. _medium: https://slmeng.medium.com/how-to-ssh-into-vmware-player-virtual-box-guest-linux-os-remotely-host-os-is-window-10-40cb348c996f
