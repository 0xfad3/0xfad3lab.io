set /A use_venv = 0
nikola >nul 2>&1 || (
:: Activate venv
set /A use_venv = 1
call venv\Scripts\activate
)

cd blog
nikola build

cd output
git init
git remote add origin git@github.com:zhgoh/zhgoh.github.io.git
git add .
git commit -m"Blog"
git push --force -u origin main
rmdir /s /Q .git

if %use_venv% EQU 1 deactivate
pause
