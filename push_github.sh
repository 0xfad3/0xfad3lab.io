#!/bin/sh
set -euo pipefail

cd blog/output
git init
git remote add origin git@github.com:zhgoh/zhgoh.github.io.git
git add .
git commit -m"Blog"
git push --force -u origin main
rm -rf .git
